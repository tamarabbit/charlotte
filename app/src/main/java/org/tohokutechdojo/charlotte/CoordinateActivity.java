package org.tohokutechdojo.charlotte;

import android.annotation.TargetApi;
import android.app.Activity;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import static android.support.v7.widget.ListPopupWindow.MATCH_PARENT;


public class CoordinateActivity extends Activity {

    private int click;
    ImageView downiv;
    ImageView topiv;
    ImageView sotai;

    final static int galleryPath = 3;
    int[] drawables = {
            R.drawable.tops,
            R.drawable.bottom1,
            R.drawable.bottom2
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinate);

        topiv = (ImageView) findViewById(R.id.tops);
        downiv = (ImageView) findViewById(R.id.bottom);
        sotai = (ImageView) findViewById(R.id.doll);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new PagerAdapter() {

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public ImageView instantiateItem(ViewGroup container, int position) {
                ImageView imageView = new ImageView(CoordinateActivity.this);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setImageDrawable(getDrawable(drawables[position]));
                imageView.setOnClickListener(
                        new View.OnClickListener(){
                            @Override
                            public void onClick(View view) {
                             topiv.setImageResource(R.drawable.tops);
                            }



                        }

                );
                container.addView(imageView, new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
                return imageView;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                if (object instanceof ImageView) {
                    ImageView imageView = (ImageView) object;
                    imageView.setImageDrawable(null);
                    container.removeView(imageView);
                }

            }

            @Override
            public int getCount() {
                return galleryPath;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        });
    }
}











