package org.tohokutechdojo.charlotte;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class CameraActivity extends AppCompatActivity {

    static final int REQUEST_CAPTURE_IMAGE = 100;
    final String SAVE_DIR = "/Charlotte/";
    File picFile;
    Uri captureUri;
    ImageView preview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSaveUri();
        Intent intent = new Intent();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, captureUri);
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_CAPTURE_IMAGE == requestCode && resultCode == Activity.RESULT_OK) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(captureUri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 10;
                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                saveBitmap(image);

                preview = (ImageView) findViewById(R.id.preview);

                if (preview != null) {
                    preview.setImageBitmap(image);
                    Intent bmpIntent = new Intent(getApplication(),CropActivity.class);
                    bmpIntent.putExtra("Bitmap", image);
                    startActivity(bmpIntent);

                } else {
                    Log.d("", "---> preview is null");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    public void setSaveUri() {
        picFile = new File(Environment.getExternalStorageDirectory().getPath() + SAVE_DIR);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
        captureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

    }
//画像の保存場所指定　captureUri＝一時的に保存する場所

    private String saveBitmap(Bitmap saveImage) throws IOException {
        try {
            if (!picFile.exists()) {
                picFile.mkdir();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            throw e;
        }

        Date date = new Date();
        SimpleDateFormat fileNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.JAPAN);
        String fileName = fileNameDate.format(date) + ".png";
        String AttachName = picFile.getAbsolutePath() + "/" + fileName;

        try {
            FileOutputStream out = new FileOutputStream(AttachName);
            saveImage.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        ContentValues values = new ContentValues();
        ContentResolver contentResolver = getContentResolver();
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put("_data", AttachName);
        contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        return AttachName;



    }









}
