package org.tohokutechdojo.charlotte;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

//extends 継承

public class MainActivity extends Activity {

    ImageButton camera_button;
    ImageButton coordinate_button;
    TextView text;


        //member変数

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // super.〇〇 スーパーの中の〇〇メソッドを呼ぶ・親クラス
        setContentView(R.layout.activity_main);
        setLayout();

    }


    private void setLayout() {

        camera_button = (ImageButton) findViewById(R.id.button);
        coordinate_button = (ImageButton) findViewById(R.id.button2);
        text = (TextView) findViewById(R.id.text);

        coordinate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CoordinateActivity.class);
                startActivity(intent);

            }

        });


        camera_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                startActivity(intent);

            }

        });


    }



}


